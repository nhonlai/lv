// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
//require('dotenv').load();

// Require keystone
var keystone = require('keystone');
var dust = require('dustjs-linkedin');
var cons = require('consolidate');
var keystoneUtils = require('keystone-utils');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({
	'name': 'LV',
	'brand': 'LV',
	'less': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'custom engine': cons.dust,
	'views': 'templates/views',
	'view engine': 'dust',
	'emails': 'templates/emails',
	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	'wysiwyg cloudinary images': true,
	'wysiwyg additional buttons': 'forecolor fontsizeselect fontselect media',
	'wysiwyg additional plugins': 'media textcolor'
});

keystone.set('cloudinary config', 'cloudinary://428734547158299:mweQTA4UEh9tk5rdFINzWDvsieY@dsdiatrnz' );

// Load your project's Models

keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});

// Load your project's Routes

keystone.set('routes', require('./routes'));
keystone.set('cors allow origin', true);

// Setup common locals for your emails. The following are required by Keystone's
// default email templates, you may remove them if you're using your own.

keystone.set('email locals', {
	logo_src: '/images/logo-email.gif',
	logo_width: 194,
	logo_height: 76,
	theme: {
		email_bg: '#f9f9f9',
		link_color: '#2697de',
		buttons: {
			color: '#fff',
			background_color: '#2697de',
			border_color: '#1a7cb7',
		},
	},
});

// Load your project's email test routes

keystone.set('email tests', require('./routes/emails'));

// Configure the navigation bar in Keystone's Admin UI

keystone.set('nav', {
	sponsorships: ['schools', 'sponsorships'],
	seminars: 'seminars',
	'news-events': 'events',
	'education-system': 'study-programs',
	visa: 'visas',
	discover: 'blogs',
	discusion: 'specialists',
	users: 'users'	
});

// Start Keystone to connect to your database and initialise the web server

keystone.start();

function cropHTML(chunk, context, bodies, params) {
	return chunk.tap(function(data) {
		return keystoneUtils.cropHTMLString(data, 200, '...', true) ;
	}).render(bodies.block, context).untap();
}
function replaceYoutubeWatchWithEmbed(chunk, context, bodies, params) {
	return chunk.tap(function(data) {
		return data.replace('watch?v=', 'embed/');
	}).render(bodies.block, context).untap();
}
function getMonth(chunk, context, bodies, params) {
	return chunk.tap(function(data) {
		return new Date(data).getMonth() + 1
	}).render(bodies.block, context).untap();
}
function getDate(chunk, context, bodies, params) {
	return chunk.tap(function(data) {
		return new Date(data).getDate()
	}).render(bodies.block, context).untap();
}
dust.helpers.cropHTML = cropHTML;
dust.helpers.replaceYoutubeWatchWithEmbed = replaceYoutubeWatchWithEmbed;
dust.helpers.getMonth = getMonth;
dust.helpers.getDate = getDate;
