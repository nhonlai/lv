﻿; jQuery(function () {

    var trackingUrlPrefix = "//product-stats.domain.com.au/v1/dreamhomes/track";
    var trackingTimeout = 500;
    var source = "domain-homepage";

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    function track(adid, listingType, eventType, position, complete) {
        var urlPost = "AdId=" + adid + "&ListingType=" + listingType + "&Source=" + source + "&EventType=" + eventType + "&position=" + position + "&nocache=" + guid();
        jQuery.ajax({
            type: "POST",
            url: trackingPrefix,
            data: urlPost,
            timeout: trackingTimeout
        }).always(function () {
            if (complete) {
                complete();
            }
        });
    }

    function getAds() {
        return jQuery.map(jQuery(".dream-homes-item"), function(a) {
            return {
                adId: jQuery(a).data("adid"),
                listingType: jQuery(a).data("listingtype")
            };
        });
    }

    // Click through tracking
    jQuery("a.dream-homes-item-link").click(function (e) {
        e.preventDefault();
        var targetUrl = this.href;
        var adId = jQuery(this).data("adid");
        var listingType = jQuery(this).data("listingtype");
        var position = jQuery(this).data("position");
        track(adId, listingType, "click", position, function () { window.location.href = targetUrl; });
    });
});