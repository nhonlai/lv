
(function ($, window, document) {
	/* ---------------------------------------------------------------------- */
	/*	Ready																  */
	/* ---------------------------------------------------------------------- */
		
	$(function () {
				
		if ($('.full-bg-image').length) {
			if($(window).width()>1024){
				$('.full-bg-image').parallax('center', 0.4);
				$('#contacts .full-bg-image').parallax('center', 0.1);
				$('#KeyVisual .full-bg-image').parallax('center', 0);
			} else {
				$('.full-bg-image').css({'background-position':'50% 50%','background-repeat':'no-repeat','background-attachment':'inherit'});
			}
		}
		
		/* ---------------------------------------------------- */
		/*	Init Progress Bar									*/
		/* ---------------------------------------------------- */
		if ($('.progress-bar').length) {
			$('.progress-bar').progressBar();
		}
		
		
		/* ---------------------------------------------------- */
		/*	Image Slider										*/
		/* ---------------------------------------------------- */
		
		if ($('.image-slider > ul').length) {
			
			var $imageslider = $('.image-slider > ul');
			
			$(window).load(function () {
				
				$imageslider.each(function (i) {

					var $this = $(this);
					
					if ($this.children('li').length < 2) { return; }

					$this.css('height', $this.children('li:first').height())
						.after('<div id="image-nav-'+ i +'" class="image-control-nav">')
						.after('<div class="image-slider-nav"><a class="prevBtn nav-prev-' + i + '">Prev</a><a class="nextBtn nav-next-' + i + '">Next</a></div>')
						.cycle({ before: function (curr, next, opts) {
							var $this = $(this);
								$this.parent().stop().animate({
								height: $this.height()
							}, opts.speed);
						},
						containerResize: false,
						easing: CONFIG.objImageSlider.easing,
						fx: 'fixedScrollHorz',
						fit: true,
						next: '.nav-next-' + i,
						pause: true,
						prev: '.nav-prev-' + i,
						slideResize: true,
						speed: CONFIG.objImageSlider.speed,
						timeout: $this.data('timeout') ? $this.data('timeout') : CONFIG.objImageSlider.timeout,
						width: '100%',
						pager: '#image-nav-' + i
					}).data('slideCount', $this.children('li').length);
					
				});

				// Pause on Nav Hover
				$('.image-slider-nav a').on('mouseenter', function () {
					$(this).parent().prev().cycle('pause');
				}).on('mouseleave', function () {
					$(this).parent().prev().cycle('resume');
				});

				// Resize
				if ($imageslider.data('slideCount') > 1) {
					$(window).on('resize', function () {
						$imageslider.css('height', $imageslider.find('li:visible').height());
					});		
				}

				// Include Swipe
				if (Modernizr.touch) {
					$imageslider.swipe({
						swipeLeft: swipeFunc,
						swipeRight: swipeFunc,
						allowPageScroll: 'auto'
					});
				}
			
			});
		}
		
	});

/* ---------------------------------------------------------------------- */
/*	Plugins																  */
/* ---------------------------------------------------------------------- */
	/* ---------------------------------------------------- */
	/*	Progress Bar										*/
	/* ---------------------------------------------------- */
	$.fn.progressBar = function(options, callback) {

		var defaults = {
			speed: 600,
			easing: 'swing'
		}, o = $.extend({}, defaults, options);

		return this.each(function() {

			var elem = $(this), methods = {};

			methods = {
				init: function () {
					this.touch = Modernizr.touch ? true : false;
					this.refreshElements();
					this.processing();
				},
				elements: {
					'.bar': 'bar',
					'.percent': 'per'
				},
				$: function(selector) { return $(selector, elem); },
				refreshElements: function () {
					for (var key in this.elements) {
						this[this.elements[key]] = this.$(key);
					}
				},
				getProgress: function() { return this.bar.data('progress'); },
				setProgress: function(self) {
					self.bar.animate({'width': self.getProgress() + '%'}, {
						duration: o.speed,
						easing: o.easing,
						step: function(progress) {
							self.per.text(Math.ceil(progress) + '%');
						},
						complete: function(scope, i, elem) {
							if (callback) {
								callback.call(this, i, elem);
							}
						}
					});
				},
				processing: function() {
					var self = this;
					if (self.touch) {
						self.setProgress(self);
					} else {
						elem.waypoint(function(direction) {
							if (direction == 'down') {
								self.setProgress(self);
							}
						}, { offset: '64%'});
					}
				}
			};
			methods.init();
		});
	};
	
	/* ---------------------------------------------------- */
	/*	Parallax											*/
	/* ---------------------------------------------------- */
	
	$.fn.parallax = function(xpos, speed) {
		var firstTop, pos;
		return this.each(function (idx, value) {
			var $this = $(value);
				if (arguments.length < 1 || xpos === null)  { xpos = "50%"; }
				if (arguments.length < 2 || speed === null) { speed = 0.4; }

			return ({
				update: function() {
					firstTop = $this.offset().top;
					pos = $(window).scrollTop();
					$this.css('backgroundPosition', xpos + " " + Math.round((firstTop - pos) * speed) + "px");
				},
				init: function() {
					var self = this;
						self.update();
					$(window).on('scroll', self.update);
				}
			}.init());
		});

	};
}(jQuery, window, document));
