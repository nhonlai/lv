
jQuery(window).load(function() {
	
	var WDW = parseInt($(window).width());
	
	// Back to Top
	$('#back-top').fadeOut();
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});
		
		$('#back-top').on('click', function (event) {
			$('html, body').animate({ scrollTop: 0 }, 800);
			event.preventDefault();
		});
		
	});
	
	if (WDW <= 768) {
		$('body').find('.truncate_textblock').removeAttr('data-read-more-height').removeAttr('class').addClass('truncate');
		$('body').find('.details-cta-wrap').fadeOut(500);
	} else {
		var DataReadMoreHeight 	= parseInt($('.readmore-js-section').attr('data-read-more-height'));
		$('.readmore-js-section').height(DataReadMoreHeight);
		$('body').find('.details-cta-wrap').fadeIn(1000);
	}
	
	/** LayerSlider **/	
	$(function () {
		var i = 0, tbH = $('div.carousel-inner').find("li");
		var lntb=tbH.length;
		for(i=0;i<lntb;i++){		
			var o = '<li data-target="#carousel-generic" data-slide-to="' + i + '" class=""></li>';
			$('ol.carousel-indicators').append(o);
			//alert(i);
		}
		$('div.carousel-inner').find('li').remove();
		$('ol.carousel-indicators').find('li:first').addClass('active');
		$('div.carousel-inner').find('div:first').addClass('active');
	});
	
})

jQuery(document).ready(function(){
	//alert($(window).height());
	
	$(window).resize(function() {
		$('#KeyVisual section.parallax').height(parseInt($(window).height()) - 135).width(parseInt($(window).width()));

		var WDW = parseInt($(window).width());

		//if(WDW < 1499) {
		//	$('#KeyVisual section.parallax').find('.container').removeAttr('style').height(parseInt($(window).height()));
		//} else {
		//	$('#KeyVisual section.parallax').find('.container').height(parseInt($(window).height())).width(parseInt($(window).width())).css({'padding':'0'});
		//}
    });
    $(window).resize();
	
	$("#page-loader .page-loader-inner").delay(1e3).fadeOut(500, function() {
        $("#page-loader").fadeOut(500);
        $('.scroll-to').delay(5000).fadeIn(800);
		
		setTimeout(function() {
			$("#KeyVisual .shield").addClass("move");
		}, 3500);
    });
	
	
	/* ---------------------------------------------------------------------- */
	/*	Select Box																  */
	/* ---------------------------------------------------------------------- */
	$(document).ready(function() {
		enableSelectBoxes();
	});
	function enableSelectBoxes() {
		$('div.selectBox').each(function(){
			$(this).children('span.selected').html($(this).children('div.selectOptions').children('span.selectOption:first').html());
			$(this).attr('value',$(this).children('div.selectOptions').children('span.selectOption:first').attr('value'));
			
			$(this).children('span.selected,span.selectArrow').click(function(){
				if($(this).parent().children('div.selectOptions').css('display') == 'none'){
					$(this).parent().children('div.selectOptions').css('display','block');
				}
				else
				{
					$(this).parent().children('div.selectOptions').css('display','none');
				}
			});
			
			$(this).find('span.selectOption').click(function(){
				$(this).parent().find('span').removeClass('dk-option-selected');
				$(this).addClass('dk-option-selected');
				$(this).parent().css('display','none');
				$(this).closest('div.selectBox').attr('value',$(this).attr('value')).attr('label',$(this).attr('label'));
				$(this).parent().siblings('select').find('option.optionSelectBox').attr('value',$(this).attr('value')).attr('label',$(this).attr('label'));
				$(this).parent().siblings('span.selected').html($(this).html());
			});
			
			$(this).find('span.selectOption').hover(
				function(){
					$(this).addClass('option-highlight');
					$(this).parent().addClass('dk-select-options-highlight');
				},
				function(){
					$(this).removeClass('option-highlight');
					$(this).parent().removeClass('dk-select-options-highlight');
				}
			);
			
		});				
	}
	
	
	$(function() {
		$( ".readmore-js-toggle a" ).on("click", function(){
			//$('body').find('.readmore-js-section').removeClass('readmore-js-collapsed');
			var DataReadMoreHeight			= parseInt($('.readmore-js-section').attr('data-read-more-height'));
			var ReadmoreJsSectionHeight		= parseInt($('.readmore-js-section').height());
			var DataReadMoreHeight_P		= parseInt($('.readmore-js-section div').height()) + 10;
			
			if (ReadmoreJsSectionHeight == 250) {
				$('body').find('.readmore-js-section').removeClass('readmore-js-collapsed').animate({height: DataReadMoreHeight_P}, "slow" );
				$(this).find('.icon').removeClass('domain-icon-ic_navigational_down_arrow').addClass('domain-icon-ic_navigational_up_arrow');
			} else {
				$('body').find('.readmore-js-section').addClass('readmore-js-collapsed').animate({height: DataReadMoreHeight}, "slow" );
				$(this).find('.icon').removeClass('domain-icon-ic_navigational_up_arrow').addClass('domain-icon-ic_navigational_down_arrow');
			}
			
			//alert(DataReadMoreHeight);
		});
	});
	
	
	
	$(function() {
		$("ul.navbar-nav").find('li').hover(function() {
			$(this).addClass('open').stop().slideDown(100);
		}, function() {
			$(this).removeClass('open').stop().slideDown(100);
		});
	});
	
	
	$(function() {
		$('a.view-more').click(function(event) {
			event.preventDefault();
			var ID = $(this).attr('href');
			$('div.product-item').find('div.item, div.sub-content').hide();
			$('div.product-item').find(ID + '-MWC, ' + ID + '-MWC div.view-full-content').show();
			$(ID + '-MWC a.close-ac').css({'display':'block'});
			$('html, body').animate({
				scrollTop: $(ID + '-MWC').offset().top - 170
			}, 1000);
		});
	});
	
	$(function() {
		$('a.close-ac').click(function(event) {
			event.preventDefault();
			var Href 	= $(this).attr('href');
			var Id 		= $(this).attr('id');
			var DataId	= $(this).attr('data-id');
			
			if (DataId === 'close-form-ask') { 
				$('body').find('div.form-ask-a-question').hide();
				$('body').find('div' + Href + '-view-ask').show();
			} else if (DataId === 'close-isi-info') {
				$('div.product-item').find('div.item, div.sub-content').show();
				$('div.product-item').find(Href + '-MWC div.view-full-content').hide();
				$(Href + '-MWC a.close-ac').removeAttr('style');
			} else if (DataId === "info-bs") {
				$('div.me-item').find('div.item').show();
				$('div.me-item').find('div' + Href + '-info').removeAttr('style');
				$('div.box-bs-ask').css({'display':'none'});
				$('div.box-bs-ask').find('div' + Href + '-view-ask').removeAttr('style');
				$('body').find('div.form-ask-a-question').hide();
				$('body').find('div.f-contact').removeAttr('style');
			} else if (DataId === "close-post-info") {
				$('body').find('div.grid').addClass('fadeOutUp');
				setTimeout(function(){
					window.location = '/' + Id;
					//$('.casestudy').load('http://digi.acuo.com.vn');
				}, 800);
			}
		});
	});

	//var highlightDates = ['1/6/2016', '20/6/2016'];
	var highlightDays = []
	var highlightMonths = []
	var highlightYears = []
	
	window["seminars"].forEach(function(item) {
		var date = new Date(item.date);
		highlightDays.push(date.getDate());
		highlightMonths.push(date.getMonth());
		highlightYears.push(date.getFullYear());
	})
	
	$('#calendar').datepicker({
		multidate: true,
		todayHighlight: true,
		weekStart: '1',
		disabled: true,
		daysOfWeekHighlighted: '0',
		beforeShowDay: function(date) {
			if (highlightDays.indexOf(date.getDate()) > -1 && 
				highlightMonths.indexOf(date.getMonth()) > -1 &&
				highlightYears.indexOf(date.getFullYear()) > -1) {
				var index = highlightDays.indexOf(date.getDate())
				
				return {
					classes: 'highlight ' + index,
					tooltip: date.getDate()
				};
			}
		}		
	});

	$('.popup-with-zoom-anim').magnificPopup({
		type: 'inline',
		preloader: true
	});
	$('.popup-with-youtube').magnificPopup({
		type: 'iframe'
	});
	$('.datepicker-days').on('click', 'td.highlight', function (e) {
		e.preventDefault();
		var index = parseInt($(this).attr('class').replace('day highlight ', ''));
		$.magnificPopup.open({
			items: {
				src: '#seminar-dialog-' + window["seminars"][index]._id
			},
			type: 'inline'
		}, 0);		
		return false;
	});
	$('.seminarLink').click(function() {
		var index = $(this).attr('id')
		$.magnificPopup.open({
			items: {
				src: '#seminar-dialog-' + index
			},
			type: 'inline'
		}, 0);
		return false;
	})
	$('#carousel.flexslider').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 300,
		itemMargin: 0,
		maxItems: 5
	});
	$('#specialistCarousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 300,
		itemMargin: 0,
		maxItems: 3
	});
	
	$('.captchaReload').on('click', function() {
		$('#captchaKey').attr('src', '/captcha.jpg?' + new Date().getTime())
	});

	var YourNamePattern     = new RegExp(/^.{3,200}$/mi);
	var SubjectPattern     	= new RegExp(/^.{3,200}$/mi);
	var MessagePattern   	= new RegExp(/^.{50,2000}$/mi);
	var PhonePattern	    = new RegExp(/^.{10,12}$/mi);
	var YourEmailPattern    = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])|(([a-z]|\d|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])([a-z]|\d|-|\.|_|~|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])*([a-z]|\d|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])))\.)+(([a-z]|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])|(([a-z]|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])([a-z]|\d|-|\.|_|~|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])*([a-z]|[0x00A0-0xD7FF0xF900-0xFDCF0xFDF0-0xFFEF])))\.?$/i);
	var VerifyPattern     	= new RegExp(/^.{4,4}$/mi);

	$('.contact-form').on('submit', function() {
		$(".errors").remove();
		var isValid = true;		

		if (!YourNamePattern.exec($("#Name").val())) {
			$(".NameError").after("<i class='errors'>(Xin vui lòng nhập tên)</i>");
			isValid = false;
		}
		if (!SubjectPattern.exec($("#Subject").val())) {
			$(".SubjectError").after("<i class='errors'>(Xin vui lòng nhập tiêu đề)</i>");
			isValid = false;
		}
		if (!YourEmailPattern.exec($("#Email").val())) {
			$(".EmailError").after("<i class='errors'>(Xin vui lòng nhập email)</i>");
			isValid = false;
		}
		if (!MessagePattern.exec($("#Message").val())) {
			$(".MessageError").after("<i class='errors'>(Xin vui lòng nhập nội dung)</i>");
			isValid = false;
		}
		if (!VerifyPattern.exec($("#Verify").val())) {
			$(".VerifyError").after("<i class='errors'>(Xin vui lòng nhập mã xác nhận)</i>");
			isValid = false;
		}

		if (isValid) {
			$.ajax({
				url: '/api/contact',
				data: $(this).serialize(),
				type: 'POST',
				success: function(response) {
					if (response.error) {
						$('#captchaKey').attr('src', '/captcha.jpg?' + new Date().getTime())
						$("<i class='errors'>(Mã xác nhận không đúng!)</i>").insertAfter(".VerifyError");
					} else {
						$('.GetInTouch').html('<div class="col-lg-12 col-xs-12 one-half text-center"><p class="p-note-oc">Thông tin liên hệ của bạn được gửi đi thành công.<br />Chúng tôi sẽ xem xét và trả lời cho bạn trong vòng 48 giờ kể từ bây giờ.<br />(*) Vui lòng kiểm tra email của bạn trong "Inbox hoặc Spam"</p></div>');
					}
				}
			});

			$(document).ajaxStart(function () {
				$("#wait").css("display", "block");
			});
			$(document).ajaxComplete(function () {
				$("#wait").css("display", "none");
			});
		}
		return false;
	});
	
	$('.specialist-form').on('submit', function() {
		$(".errors").remove();
		var isValid = true;

		if (!YourNamePattern.exec($("#Name").val())) {
			$(".NameError").after("<i class='errors'>(Xin vui lòng nhập tên)</i>");
			isValid = false;
		}
		if (!YourEmailPattern.exec($("#Email").val())) {
			$(".EmailError").after("<i class='errors'>(Xin vui lòng nhập email)</i>");
			isValid = false;
		}
		if (!PhonePattern.exec($("#Phone").val())) {
			$(".PhoneError").after("<i class='errors'>(Xin vui lòng nhập số điện thoại)</i>");
			isValid = false;
		}
		if (!MessagePattern.exec($("#Message").val())) {
			$(".MessageError").after("<i class='errors'>(Xin vui lòng nhập nội dung)</i>");
			isValid = false;
		}
		if (!VerifyPattern.exec($("#Verify").val())) {
			$(".VerifyError").after("<i class='errors'>(Xin vui lòng nhập mã xác nhận)</i>");
			isValid = false;
		}
		if (isValid) {
			$.ajax({
				url: '/api/contact',
				data: $(this).serialize(),
				type: 'POST',
				success: function (response) {
					if (response.error) {
						$('#captchaKey').attr('src', '/captcha.jpg?' + new Date().getTime())
						$("<i class='errors'>(Mã xác nhận không đúng!)</i>").insertAfter(".VerifyError");
					} else {
						$('.GetInTouch').html('<div class="col-lg-12 col-xs-12 one-half text-center"><p class="p-note-oc">Thông tin liên hệ của bạn được gửi đi thành công.<br />Chúng tôi sẽ xem xét và trả lời cho bạn trong vòng 48 giờ kể từ bây giờ.<br />(*) Vui lòng kiểm tra email của bạn trong "Inbox hoặc Spam"</p></div>');
					}
				}
			});

			$(document).ajaxStart(function () {
				$("#wait").css("display", "block");
			});
			$(document).ajaxComplete(function () {
				$("#wait").css("display", "none");
			});
		}
		return false;
	});

	$('.studyCarousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 300,
		itemMargin: 0,
		maxItems: 3
	});
	$('.seminarCarousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 290,
		itemMargin: 0,
		maxItems: 5
	});

	$('.readmoreEducation').click(function() {
		window.location.href = '/school?active=' + $('#portfolio-filter .active').attr('data-filter').replace('\.', '')
	});

	$('.popup-with-zoom-anim').on('click', function() {
    $(".errors").remove();
	})
	$('.register-form').on('submit', function() {
    $(".errors").remove();
    var isValid = true;

    if (!YourNamePattern.exec($("#registerName").val())) {
      $("#registerName").after("<i class='errors'>(Xin vui lòng nhập tên)</i>");
      isValid = false;
    }
    var birthDate = parseInt($("#registerBirthDate").val())
    if (!birthDate || birthDate < 1 || birthDate > 31) {
      $(".registerBirth").after("<i class='errors'>(Xin vui lòng nhập ngày tháng năm sinh)</i>");
      isValid = false;
    } else {
      var birthMonth = parseInt($("#registerBirthMonth").val())
      if (!birthMonth || birthMonth < 1 || birthMonth > 12) {
        $(".registerBirth").after("<i class='errors'>(Xin vui lòng nhập ngày tháng năm sinh)</i>");
        isValid = false;
      } else {
        var birthYear = parseInt($("#registerBirthYear").val())
        if (!birthYear || birthYear < 1900) {
          $(".registerBirth").after("<i class='errors'>(Xin vui lòng nhập ngày tháng năm sinh)</i>");
          isValid = false;
        }
			}
		}
    if (!PhonePattern.exec($("#registerPhone").val())) {
      $("#registerPhone").after("<i class='errors'>(Xin vui lòng nhập số điện thoại)</i>");
      isValid = false;
    }
    if (!YourEmailPattern.exec($("#registerEmail").val())) {
      $("#registerEmail").after("<i class='errors'>(Xin vui lòng nhập email)</i>");
      isValid = false;
    }
    if (!SubjectPattern.exec($("#registerAddress").val())) {
      $("#registerAddress").after("<i class='errors'>(Xin vui lòng nhập địa chỉ)</i>");
      isValid = false;
    }
    if (!MessagePattern.exec($("#registerMessage").val())) {
      $("#registerMessage").after("<i class='errors'>(Xin vui lòng nhập lời nhắn)</i>");
      isValid = false;
    }
    if (isValid) {
      $.ajax({
        url: '/api/register',
        data: $(this).serialize(),
        type: 'POST',
        success: function(response) {
          $('.registerContent').html('<div class="col-lg-12 col-xs-12 one-half text-center"><p class="p-note-oc">Thông tin đăng ký của bạn được gửi đi thành công.</p></div>');
        }
      });

      $(document).ajaxStart(function () {
        $("#wait").css("display", "block");
      });
      $(document).ajaxComplete(function () {
        $("#wait").css("display", "none");
      });
    }
    return false;
  });
});




	
