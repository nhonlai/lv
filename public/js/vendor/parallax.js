
jQuery(document).ready(function(){
	
	(function ($, window, Modernizr, document, CONFIG) {
		if ($('.full-bg-image').length) {
			$('.full-bg-image').parallax('center', 0.4);
			$('#KeyVisual .full-bg-image').parallax('center', 0);
		}
		
		$.fn.parallax = function(xpos, speed) {
			var firstTop, pos;
			return this.each(function (idx, value) {
	
				var $this = $(value);
					if (arguments.length < 1 || xpos === null)  { xpos = "50%"; }
					if (arguments.length < 2 || speed === null) { speed = 0; }
	
				return ({
					update: function() {
						firstTop = $this.offset().top;
						pos = $(window).scrollTop();
						console.log(speed)
						$this.css('backgroundPosition', xpos + " " + Math.round((firstTop - pos) * speed) + "px");
					},
					init: function() {
						var self = this;
							self.update();
						$(window).on('scroll', self.update);
					}
				}.init());
			});
		}
	}(jQuery, window, Modernizr, document, CONFIG));
	
	/*$.fn.parallax = function(xpos, speed) {
		var firstTop, pos;
		return this.each(function (idx, value) {

			var $this = $(value);
				if (arguments.length < 1 || xpos === null)  { xpos = "50%"; }
				if (arguments.length < 2 || speed === null) { speed = 0.4; }

			return ({
				update: function() {
					firstTop = $this.offset().top;
					pos = $(window).scrollTop();
					$this.css('backgroundPosition', xpos + " " + Math.round((firstTop - pos) * speed) + "px");
				},
				init: function() {
					var self = this;
						self.update();
					$(window).on('scroll', self.update);
				}
			}.init());
		});

	};
		
	if (!Modernizr.touch) {
				if ($('.full-bg-image').length) {
					$('.full-bg-image').parallax('center', 0.4);
				}
			}	*/
	
	// The plugin code
	/*(function($){
		$.fn.parallax = function(options){
			var $$ = $(this);
			offset = $$.offset();
			var firstTop, pos;
			var defaults = {
				"start": 0,
				"stop": offset.top + $$.height(),
				"coeff": 0.1
			};
			var timer = 0;
			var opts = $.extend(defaults, options);
			var func = function(){
				timer = 0;
				$(window).bind('scroll', function() {
					windowTop = $(window).scrollTop();
					if((windowTop >= opts.start) && (windowTop <= opts.stop)) {
						newCoord = windowTop * opts.coeff;
						$$.css({
							"background-position": "50% "+ newCoord + "px"
						});
					}
					
				});
			};
			return this.each(function(){
				$(window).bind('scroll', function() {
					window.clearTimeout(timer);
					timer = window.setTimeout(func, 1);
				});
			});
		};
	})(jQuery);*/
	
	// call the plugin
	//$('.section').parallax({ "coeff":-0.65 });
	//$('.parallax-bg-1 .parallax-section').parallax('center', 0);
	//$('.parallax-bg-2 .parallax-section').parallax('center', 0.1);
	
});
