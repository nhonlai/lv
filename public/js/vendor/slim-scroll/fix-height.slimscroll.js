jQuery(window).resize(function(){    
	$('.inner-list-store').slimScroll({
		height: $('.inner-list-store').css({'height':(($('.wtb').height())-155)+'px'})
	});
});

jQuery(document).ready(function() {	
	
	$(function(){		
		/** Scroll Item Delivery **/
		$('.scrol-item-d').slimScroll({wheelStep: 20});
		$('.scrol-item-d').slimScroll({height: '100px'});
		$('.scrol-item-cart').slimScroll({height: '190px'});
		$('.scrol-cus-item').slimScroll({height: '281px'});
		//$('.inner-list-store').slimScroll({height: '400px'});
		$('.ils-user').slimScroll({height: '200px'});
		$('.list-item').slimScroll({height: '230px'});
		$('.scrol-wrapper').slimScroll({height: '310px'});
	});
	
	var windowWidth = $(window).width();
	if (windowWidth <= 480) {
		$('div.takara div.scrol-wrapper').slimScroll({height: '250px'});
	}
	
});