var keystone = require('keystone');
var Types = keystone.Field.Types;

var Sponsorship = new keystone.List('Sponsorship', {
	map: { name: 'title' },
	defaultSort: '-createdAt'
});

Sponsorship.add({
	title: { type: String , required: true },
	school: { type: Types.Relationship, ref: 'School', many: true },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
	createdAt: { type: Date, default: Date.now, hidden: true }
});

Sponsorship.defaultColumns = 'title, school, createdAt|20%';
Sponsorship.register();
