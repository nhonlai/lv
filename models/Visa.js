var keystone = require('keystone'),
	Types = keystone.Field.Types;

var Visa = new keystone.List('Visa', {
	autokey: { path: 'slug', from: 'title', unique: true },
	map: { name: 'title' },
	defaultSort: '-createdAt'
});

Visa.add({
	title: { type: String, required: true },
	type: { type: Types.Select, options: 'travel, study, work', default: 'study' },
	thumbnail: { type: Types.CloudinaryImage },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
	createdAt: { type: Date, default: Date.now, hidden: true }
});

Visa.defaultColumns = 'title, content, createdAt';
Visa.register();
