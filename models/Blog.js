var keystone = require('keystone'),
	Types = keystone.Field.Types;

var Blog = new keystone.List('Blog', {
	autokey: { path: 'slug', from: 'title', unique: true },
	map: { name: 'title' },
	defaultSort: '-createdAt'
});

Blog.add({
	title: { type: String, required: true },
	thumbnail: { type: Types.CloudinaryImage },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
	createdAt: { type: Date, default: Date.now, hidden: true }
});

Blog.defaultColumns = 'title, content, createdAt';
Blog.register();
