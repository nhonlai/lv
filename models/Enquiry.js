var keystone = require('keystone');
var Types = keystone.Field.Types;

var Enquiry = new keystone.List('Enquiry', {
	nocreate: true,
	noedit: true,
});

Enquiry.add({
	name: { type: String, required: true },
	email: { type: Types.Email, required: true },
	phone: { type: String },
	subject: { type: String },
	message: { type: String, required: true },
	type: { type: String },
	specialistEmail: { type: Types.Email },
	createdAt: { type: Date, default: Date.now, hidden: true },
  address: { type: String },
  birthday: { type: String },
  source: { type: String },
});

Enquiry.schema.pre('save', function(next) {
	this.wasNew = this.isNew;
	next();
});

Enquiry.schema.post('save', function() {
	if (this.wasNew) {
		this.sendNotificationEmail();
	}
});

var emails = require('../routes/emails');

Enquiry.schema.methods.sendNotificationEmail = function(callback) {
	if ('function' !== typeof callback) {
		callback = function() {};
	}

	var enquiry = this;
	
	if (enquiry.type === 'contact') {
		keystone.list('User').model.find().where('isAdmin', true).exec(function(err, admins) {
			if (err) return callback(err);

			var toEmails = []
			admins.forEach(function(admin) {
				toEmails.push(admin.email)
			});

			emails.sendEmail('contact', {
				from: 'contact.web.lv@gmail.com',
				to: toEmails.join(','),
				subject: 'Liên hệ từ website LV',
				data: enquiry
			});
		});
	} else if (enquiry.type === 'specialist') {
		emails.sendEmail('contact', {
			from: 'contact.web.lv@gmail.com',
			to: enquiry.specialistEmail,
			subject: 'Câu hỏi cho chuyên gia từ website LV',
			data: enquiry
		});
	} else if (enquiry.type === 'register') {
    emails.sendEmail('register', {
      from: 'contact.web.lv@gmail.com',
			to: enquiry.email,
      subject: 'Đăng ký thành công',
			data: enquiry
    });
	}
};

Enquiry.defaultSort = '-createdAt';
Enquiry.defaultColumns = 'name, email, type, createdAt';
Enquiry.register();
