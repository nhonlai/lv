var keystone = require('keystone');
var Types = keystone.Field.Types;

var Setting = new keystone.List('Setting', {
	nocreate: true,
	nodelete: true
});

Setting.add({
	name: {type: String, required: true, index: true},
	introShortContent: {type: Types.Html, wysiwyg: true, height: 200},
	introContent: {type: Types.Html, wysiwyg: true, height: 400},
	mainBackgroundImages: {type: Types.CloudinaryImages, label: 'Main Background Images (1920x850)'},
	mainBackgroundMobileImages: {type: Types.CloudinaryImages, label: 'Main Background Mobile Images (750x850)'},
	discussionBackgroundImage: {type: Types.CloudinaryImage },
	contactBackgroundImage: {type: Types.CloudinaryImage }
});
	
Setting.register();
