var keystone = require('keystone');
var Types = keystone.Field.Types;

var School = new keystone.List('School', {
	defaultSort: '-createdAt'
});

School.add({
	name: { type: String, required: true },
	type: { type: Types.Select, options: 'english, secondary, college, university, postgraduate', default: 'university' },
	logo: { type: Types.CloudinaryImage },
	thumbnail: { type: Types.CloudinaryImage },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
	createdAt: { type: Date, default: Date.now, hidden: true }
});
School.relationship({ ref: 'Sponsorship', path: 'school' });

School.defaultColumns = 'name, content, screatedAt';
School.register();
