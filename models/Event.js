var keystone = require('keystone'),
	Types = keystone.Field.Types;

var Event = new keystone.List('Event', {
	autokey: { path: 'slug', from: 'title', unique: true },
	map: { name: 'title' },
	defaultSort: '-createdAt'
});

Event.add({
	title: { type: String, required: true },
	thumbnail: { type: Types.CloudinaryImage },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
	createdAt: { type: Date, default: Date.now, hidden: true }
});

Event.defaultColumns = 'title, content, createdAt';
Event.register();
