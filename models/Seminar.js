var keystone = require('keystone');
var Types = keystone.Field.Types;

var Seminar = new keystone.List('Seminar', {
	autokey: { from: 'name', path: 'key', unique: true },
	defaultSort: '-date'
});

Seminar.add({
	name: { type: String, required: true },
	date: { type: Date, required: true, default: Date.now },
	content: { type: Types.Html, wysiwyg: true, height: 400 },	
	createdAt: { type: Date, default: Date.now, hidden: true }
});

Seminar.defaultColumns = 'name, date';
Seminar.register();
