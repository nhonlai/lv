var keystone = require('keystone');
var Types = keystone.Field.Types;

var StudyProgram = new keystone.List('StudyProgram', {
	defaultSort: '-createdAt'
});

StudyProgram.add({
	name: { type: String, required: true },
	type: { type: Types.Select, options: 'intro, english, high-school, english, pre-university, college, university, postgraduate', default: 'university' },
	thumbnail: { type: Types.CloudinaryImage },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
	createdAt: { type: Date, default: Date.now, hidden: true }
});

StudyProgram.register();
