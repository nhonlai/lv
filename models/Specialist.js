var keystone = require('keystone');
var Types = keystone.Field.Types;

var Specialist = new keystone.List('Specialist', {
	autokey: { from: 'name', path: 'key', unique: true },
	defaultSort: '-name'
});

Specialist.add({	
	name: { type: String, required: true },
	email: { type: Types.Email },
	photo: { type: Types.CloudinaryImage },
	description: { type: Types.Html, wysiwyg: true, height: 150 }	
});

Specialist.defaultColumns = 'name, email';
Specialist.register();
