var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'seminar';

	view.query('seminars', keystone.list('Seminar').model.find().sort({date: -1 }));

	// Render the view
	view.render('seminarList');
};
