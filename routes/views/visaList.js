var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'visa';
	var typeList = ['travel', 'study', 'work']
	var reqType = req.params.type
	if (typeList.indexOf(reqType) === -1) {
		reqType = 'study'
	}
	locals.type = reqType

	view.query('visas', keystone.list('Visa').model.find({
		type: reqType
	}).sort({createdAt: -1 }));

	// Render the view
	view.render('visaList');
};
