var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'sponsorship';

	view.query('sponsorships', keystone.list('Sponsorship').model.find().sort('sortOrder').populate('school'));

	// Render the view
	view.render('sponsorshipList');
};
