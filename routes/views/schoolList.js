var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;
	console.log(req.query)

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'educationSystem';
	locals.query = req.query.active;

	view.query('schools', keystone.list('School').model.find().sort({name: 1 }));

	// Render the view
	view.render('schoolList');
};
