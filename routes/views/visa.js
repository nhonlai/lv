var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'visa';
	locals.redirectUrl = '/visa/' + req.params.type;
	locals.type = req.params.type;

	keystone.list('Visa').model.find({
		_id: req.params.id
	}).exec().then(function(data) {
		locals.data = data;

		//view.query('data', data);
		view.query('next', keystone.list('Visa').model.find({
			_id: {$gt: req.params.id},
			type: data.type
		}).sort({_id: 1 }).limit(1));
		view.query('prev', keystone.list('Visa').model.find({
			_id: {$lt: req.params.id},
			type: data.type
		}).sort({_id: 1 }).limit(1));

		// Render the view
		view.render('single');
	});
};
