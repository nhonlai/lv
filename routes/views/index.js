var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');
var async = require('async');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';

	//view.query('sponsorships', keystone.list('Sponsorship').model.find().sort('sortOrder').populate('school'));
	//view.query('seminars', keystone.list('Seminar').model.find());
	view.query('specialists', keystone.list('Specialist').model.find());
	view.query('pageSettings', keystone.list('Setting').model.find());
	//view.query('blogs', keystone.list('Blog').model.find().sort({createdAt: 1 }).limit(3));

	async.parallel([
		function(callback) {
			keystone.list('School').model.find({
				type: 'english'
			}).sort({name: 1}).limit(10).exec().then(function(schools) {
				callback(null, schools);
			});
		},
		function(callback) {
			keystone.list('School').model.find({
				type: 'secondary'
			}).sort({name: 1}).limit(10).exec().then(function(schools) {
				callback(null, schools);
			});
		},
		function(callback) {
			keystone.list('School').model.find({
				type: 'college'
			}).sort({name: 1}).limit(10).exec().then(function(schools) {
				callback(null, schools);
			});
		},
		function(callback) {
			keystone.list('School').model.find({
				type: 'university'
			}).sort({name: 1}).limit(10).exec().then(function(schools) {
				callback(null, schools);
			});
		},
		function(callback) {
			keystone.list('School').model.find({
				type: 'postgraduate'
			}).sort({name: 1}).limit(10).exec().then(function(schools) {
				callback(null, schools);
			});
		}
	], function(err, result) {
		locals.schools = result[0].concat(result[1]).concat(result[2]).concat(result[3]).concat(result[4])
		console.log(locals.schools)
	});

	// Render the view
	view.render('index');
};
