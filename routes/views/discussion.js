var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'discussion';
	locals.redirectUrl = '#discussion'

	view.query('data', keystone.list('Specialist').model.findById(req.params.id))

	// Render the view
	view.render('discussion');
};
