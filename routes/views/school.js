var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'educationSystem';
	locals.redirectUrl = '/school'

	view.query('data', keystone.list('School').model.find({
		_id: req.params.id
	}));
	view.query('next', keystone.list('School').model.find({
		_id: {$gt: req.params.id}
	}).sort({_id: 1 }).limit(1));
	view.query('prev', keystone.list('School').model.find({
		_id: {$lt: req.params.id}
	}).sort({_id: 1 }).limit(1));

	// Render the view
	view.render('single');
};
