var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'studyProgram';
	var typeList = ['intro', 'english', 'high-school', 'english', 'pre-university', 'college', 'university', 'postgraduate']
	var reqType = req.params.type
	if (typeList.indexOf(reqType) === -1) {
		reqType = 'university'
	}
	locals.type = reqType

	view.query('studyPrograms', keystone.list('StudyProgram').model.find({
		type: reqType
	}).sort({createdAt: -1 }));

	//view.query('highSchoolList', keystone.list('StudyProgram').model.find({
	//	type: 'high-school'
	//}));
	//view.query('englishList', keystone.list('StudyProgram').model.find({
	//	type: 'english'
	//}));
	//view.query('preUniversityList', keystone.list('StudyProgram').model.find({
	//	type: 'pre-university'
	//}));
	//view.query('universityList', keystone.list('StudyProgram').model.find({
	//	type: 'university'
	//}));
	//view.query('postgraduateList', keystone.list('StudyProgram').model.find({
	//	type: 'postgraduate'
	//}));

	// Render the view
	view.render('studyProgramList');
};
