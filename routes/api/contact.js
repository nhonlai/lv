var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');

exports = module.exports = function (req, res) {
	if (req.session.captcha != parseInt(req.body.captchaKey)) {
		res.apiResponse({
			error: true
		});
	} else {
		var newEnquiry = new Enquiry.model();
		var updater = newEnquiry.getUpdateHandler(req);
	
		updater.process(req.body, {
			flashErrors: true,
			fields: 'name, email, subject, phone, message, type, specialistEmail',
			errorMessage: 'There was a problem submitting your enquiry:',
		}, function (err) {
			if (err) {
				console.log(err)
				res.apiResponse({
					error: true
				});
			} else {
				res.apiResponse({
					error: false
				});
			}
		});
	}
}
