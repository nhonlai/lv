var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');

exports = module.exports = function (req, res) {
  var newEnquiry = new Enquiry.model();
  var updater = newEnquiry.getUpdateHandler(req)

	req.body.name = req.body.registerName
	req.body.email = req.body.registerEmail
	req.body.phone = req.body.registerPhone
	req.body.address = req.body.registerAddress
	req.body.source = req.body.registerSource
	req.body.message = req.body.registerMessage
	req.body.birthday = req.body.registerBirthDate + '-' + req.body.registerBirthMonth + '-' + req.body.registerBirthYear

  updater.process(req.body, {
    flashErrors: true,
    fields: 'name, email, phone, message, type, address, birthday, source',
    errorMessage: 'There was a problem submitting your enquiry:',
  }, function (err) {
    if (err) {
      console.log(err)
      res.apiResponse({
        error: true
      });
    } else {
      res.apiResponse({
        error: false
      });
    }
  });
}
