/**
 * This file defines the email tests for your project.
 *
 * Each email test should provide the locals used to render the
 * email template for preview.
 *
 * Values can either be an object (for simple tests), or a
 * function that calls a callback(err, locals).
 *
 * Sample generated emails, based on the keys and methods below,
 * can be previewed at /keystone/test-email/{key}
 */

var keystone = require('keystone');
var nodemailer = require('nodemailer');
var ejs = require('ejs');
var fs = require('fs');

module.exports = {
	
	/** New Enquiry Notifications */

	'enquiry-notification': function(req, res, callback) {
		
		// To test enquiry notifications we create a dummy enquiry that
		// is not saved to the database, but passed to the template.
		
		//var Enquiry = keystone.list('Enquiry');
        //
		//var newEnquiry = new Enquiry.model({
		//	name: { first: 'Test', last: 'User' },
		//	email: 'contact@lv.com',
		//	phone: '+61 2 1234 5678',
		//	enquiryType: 'message',
		//	message: { md: 'Nice enquiry notification.' },
		//});
        //
		//callback(null, {
		//	admin: 'Admin User',
		//	enquiry: newEnquiry,
		//	enquiry_url: '/keystone/enquiries/',
		//});
	},
	sendEmail: function(emailName, params, callback) {
		fs.readFile(__dirname + '/../templates/emails/' + emailName + '.ejs', 'utf8', function (err, tpl) {
			var html  = ejs.render(tpl, params.data);

			var smtpTransport = nodemailer.createTransport('smtps://contact.web.lv%40gmail.com:sar1tasa@smtp.gmail.com');

			smtpTransport.sendMail({
				from        : params.from,
				to          : params.to,
				subject     : params.subject,
				html        : html
			}, function (error, responseStatus) {
				if (error) {
					console.log('Sent email fail: ' + params.data._id);
					console.log(error);
				} else {
					console.log('Sent email success: ' + params.data._id);
				}
				if (callback) {
					callback();
				}
			});
		});
	}	
};
