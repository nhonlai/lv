/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);
var captcha = require('captcha');

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	api: importRoutes('./api')
};

// Setup Route Bindings
exports = module.exports = function (app) {
	app.use(captcha({ 
		url: '/captcha.jpg', 
		codeLength: 4,
		fontSize: 20,
		canvasWidth: 90,
		canvasHeight: 38
	})); // captcha params
	// Views
	app.get('/', routes.views.index);
	app.get('/studyProgram/:type', routes.views.studyProgramList);
	app.get('/studyProgram/:type/:id', routes.views.studyProgram);
	app.get('/visa/:type', routes.views.visaList);
	app.get('/visa/:type/:id', routes.views.visa);
	app.get('/sponsorship', routes.views.sponsorshipList);
	app.get('/sponsorship/:id', routes.views.sponsorship);
	app.get('/seminar', routes.views.seminarList);
	app.get('/discussion/:id', routes.views.discussion);
	app.get('/school', routes.views.schoolList);
	app.get('/school/:id', routes.views.school);
	app.get('/blog', routes.views.blogList);
	app.get('/blog/:id', routes.views.blog);
	app.get('/event', routes.views.eventList);
	app.get('/event/:id', routes.views.event);
	app.get('/specialist/:id', routes.views.specialist);
	// API
	//app.all('/api*', keystone.middleware.cors);
	app.post('/api/contact', keystone.middleware.api, routes.api.contact);
	app.post('/api/register', keystone.middleware.api, routes.api.register);

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);
};
